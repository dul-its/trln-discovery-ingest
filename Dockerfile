ARG ruby_version="3.0"
ARG builder="builder"
ARG bundle="bundle"

FROM ruby:${ruby_version} AS builder

SHELL ["/bin/bash", "-c"]

ENV APP_ROOT="/opt/app-root" \
    APP_USER="app-user" \
    APP_UID="1001" \
    APP_GID="0" \
    BUNDLE_USER_HOME="${GEM_HOME}" \
    DATA_VOLUME="/data" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    RAILS_ENV="production" \
    RAILS_PORT="3000" \
    TZ="US/Eastern"

WORKDIR $APP_ROOT

RUN set -eux; \
    apt-get -y update; \
    apt-get -y install jq libxml2-utils nodejs npm postgresql-client; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*; \
    npm install -g yarn; \
    useradd -r -u $APP_UID -g $APP_GID -d $APP_ROOT -s /sbin/nologin $APP_USER; \
    git config --system --add safe.directory "*"

#---------------------------+

FROM ${builder} AS bundle

COPY .ruby-version Gemfile Gemfile.lock ./

RUN gem install bundler -v "$(tail -1 Gemfile.lock | awk '{print $1}')" && \
    bundle install && \
    chmod -R g=u $GEM_HOME

#---------------------------+

FROM ${bundle} AS app

ARG APP_VERSION="0.0.0"
ARG APP_VCS_REF="0"
ARG BUILD_DATE="1970-01-01T00:00:00Z"

# Setting HOME makes OKD happy
ENV HOME="${APP_ROOT}" \
    APP_VERSION="${APP_VERSION}" \
    APP_VCS_REF="${APP_VCS_REF}"

LABEL org.opencontainers.artifact.description="Administrative app for ingesting Duke catalog content into TRLN shared catalog"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/dul-its/trln-discovery-ingest"
LABEL org.opencontainers.image.version="${APP_VERSION}"
LABEL org.opencontainers.image.revision="${APP_VCS_REF}"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.license="BSD-3-Clause"
LABEL org.opencontainers.image.created="${BUILD_DATE}"

COPY . .

RUN mkdir -p $DATA_VOLUME && \
    chmod -R g=u . $DATA_VOLUME

USER $APP_USER

RUN SECRET_KEY_BASE=1 ./bin/rails assets:precompile

VOLUME $DATA_VOLUME

EXPOSE $RAILS_PORT

CMD ["./bin/rails", "server"]
