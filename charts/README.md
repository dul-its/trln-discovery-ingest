# trln-discovery-ingest OKD4 Helm chart

Values to set:

    ingestRoutesAllowFromIPs
    secretKeyBase
    oauth-proxy.cookieSecret
    postgres.databasePassword
    postgres.databaseUser
    postgres.databaseName
    spoffordConfig.accountName
    spoffordConfig.token
