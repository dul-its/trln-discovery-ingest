namespace :tdi do
  desc 'Weed ingest records older than a certain number of days'
  task :weed, [:days] => :environment do |_t, args|
    Ingest.weed(args[:days])
  end
end
