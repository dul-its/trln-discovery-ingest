module TrlnDiscoveryIngest
  VERSION = format('%s+%s', ENV.fetch('APP_VERSION', '0.0.0'), ENV.fetch('APP_VCS_REF', 'unknown'))

  META = {
    tdi: {
      version: VERSION
    },
    marc_to_argot: {
      version: MarcToArgot::VERSION
    },
    argot: {
      version: Argot::VERSION
    },
    spofford_client: {
      version: Spofford::Client::VERSION
    }
  }.freeze

  def self.meta_versions
    META.map { |k, v| "#{k} #{v[:version]}" }
  end
end
