#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

docker-compose -p td-ingest-development \
	       -f docker-compose.yml \
	       -f docker-compose.development.yml \
	       "$@"
