#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

docker-compose -p td-ingest-test \
	       -f docker-compose.yml \
	       -f docker-compose.test.yml \
	       "$@"
