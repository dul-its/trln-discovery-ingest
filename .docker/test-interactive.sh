#!/bin/bash

context="$(git rev-parse --show-toplevel)"

cd "$(dirname ${BASH_SOURCE[0]})"

./test.sh run --rm -v "${context}:/opt/app-root" app bash

./test.sh down
