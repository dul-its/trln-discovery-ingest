{{/*
Expand the name of the chart.
*/}}
{{- define "trln-discovery-ingest.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "trln-discovery-ingest.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "trln-discovery-ingest.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "trln-discovery-ingest.labels" -}}
helm.sh/chart: {{ include "trln-discovery-ingest.chart" . }}
{{ include "trln-discovery-ingest.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "trln-discovery-ingest.selectorLabels" -}}
app.kubernetes.io/name: {{ include "trln-discovery-ingest.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "trln-discovery-ingest.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "trln-discovery-ingest.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Image stream tag
*/}}
{{- define "trln-discovery-ingest.imageStreamTag" -}}
{{- printf "%s-%s" .Values.image.stream.name .Values.image.stream.tag }}
{{- end }}

{{/*
Database URL
*/}}
{{- define "trln-discovery-ingest.databaseUrl" -}}
{{- printf "postgresql://%s:%s@%s.%s.svc.cluster.local:5432/%s"
            .Values.global.postgresql.auth.username
            .Values.global.postgresql.auth.password
	    .Values.postgresql.fullnameOverride
            .Release.Namespace
            .Values.global.postgresql.auth.database }}
{{- end }}

{{- define "trln-discovery-ingest.appImage" -}}
{{- printf "%s/%s/%s:%s"
    	   .Values.image.registry
	   .Release.Namespace
	   .Values.image.name
           .Values.image.tag }}
{{- end }}