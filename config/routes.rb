Rails.application.routes.draw do
  root to: 'status#index'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/queue'

  resources :add_update_ingests, only: :create
  resources :delete_ingests, only: :create
  resources :status, only: :index, defaults: { format: 'json' }

  post '/ingest/duke', to: 'mock#create'
end
