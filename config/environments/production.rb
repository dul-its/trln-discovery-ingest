Rails.application.configure do
  config.cache_classes = true
  config.eager_load = true
  config.consider_all_requests_local = false
  config.action_controller.perform_caching = true
  config.public_file_server.enabled = true
  config.active_storage.service = :local
  config.action_mailer.perform_caching = false
  config.active_support.deprecation = :notify
  config.active_record.dump_schema_after_migration = false
end
