RailsAdmin.config do |config|
  config.asset_source = :sprockets

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    export
    show
    show_in_app
  end

  config.navigation_static_links = {
    'Status' => '/status.html',
    'Queue' => '/queue',
    'API' => 'https://gitlab.oit.duke.edu/dul-its/trln-discovery-ingest/blob/master/API.md',
    'Builds' => 'https://gitlab.oit.duke.edu/dul-its/trln-discovery-ingest/pipelines',
    'OKD Console' => 'https://console.apps.prod.okd4.fitz.cloud.duke.edu/k8s/ns/dul-trln-discovery-ingest-prod/deployments/app/',
    'TRLN Ingest Server' => 'https://ingest.discovery.trln.org/'
  }

  config.compact_show_view = false

  config.main_app_name = "DUL TRLN Discovery Ingest v#{TrlnDiscoveryIngest::VERSION}"
end
