require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
# require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

lib = File.expand_path('../lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'trln_discovery_ingest'

module TrlnDiscoveryIngest
  class Application < Rails::Application
    # config.load_defaults 5.2
    config.api_only = true
    config.active_job.queue_adapter = :sidekiq
    config.colorize_logging = false
    config.i18n.fallbacks = true
    config.time_zone = 'Eastern Time (US & Canada)'

    # Middleware required by RailsAdmin
    config.middleware.use ActionDispatch::Cookies
    config.middleware.use ActionDispatch::Flash
    config.middleware.use Rack::MethodOverride
    config.middleware.use ActionDispatch::Session::CookieStore, { key: '_trln_discovery_ingest_session' }

    # Logging
    config.log_level     = ENV.fetch('RAILS_LOG_LEVEL', 'info').to_sym
    config.log_formatter = ::Logger::Formatter.new
    logger               = ActiveSupport::Logger.new(STDOUT)
    logger.formatter     = config.log_formatter
    config.logger        = ActiveSupport::TaggedLogging.new(logger)
  end
end
