SHELL = /bin/bash

build_tag ?= trln-discovery-ingest

bundle_command = docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) bundle

.PHONY : build
build:
	./build.sh

.PHONY : clean
clean:
	rm -f ./log/*.log
	rm -rf ./storage/*
	rm -rf ./tmp/*

.PHONY : test
test:
	./.docker/test.sh up --exit-code-from=app; \
	code=$$?; \
	./.docker/test.sh down; \
	exit $$code

.PHONY: lock
lock:
	$(bundle_command) lock

.PHONY: audit
audit:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) ./audit.sh

.PHONY: update
update:
	$(bundle_command) update $(args)

.PHONY: rubocop
rubocop:
	$(bundle_command) exec rubocop $(args)

.PHONY: autocorrect
autocorrect:
	$(bundle_command) exec rubocop -a

.PHONY: regenerate-todo
regenerate-todo:
	$(bundle_command) exec rubocop --regenerate-todo
