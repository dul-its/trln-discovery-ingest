class CreateIngestTable < ActiveRecord::Migration[5.2]
  def change
    create_table :ingests do |t|
      t.string :type
      t.integer :records
      t.integer :status, default: 0
      t.timestamps
    end
  end
end
