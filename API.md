# TRLN Discovery Ingest API

## Add/Update endpoint

#### Request

    POST /add_update_ingests
    Content-Type: text/xml, application/xml

Send MARC XML file as attachment bound to `infile` parameter.

#### Response

    Status: 201 (Success)
    Content-Type: application/json

#### Example

    $ curl -F infile=@/path/to/marc.xml https://HOSTNAME/add_update_ingests

## Delete endpoint

#### Request

    POST /delete_ingests
    Content-Type: text/plain

Send plain text file of record identifiers, one per line, as attachment bound to `infile` parameter.

#### Response

    Status: 201 (Success)
    Content-Type: application/json

#### Example

    $ curl -F infile=@/path/to/file.txt https://HOSTNAME/delete_ingests

## Status endpoint

#### Request

    GET /status

#### Response

    Status: 200 OK
    Content-Type: application/json

#### Example

    $ curl https://HOSTNAME/status

```json
{
  "meta": {
    "tdi": {
      "version": "1.0.0.pre"
    },
    "marc_to_argot": {
      "version": "0.3.0"
    },
    "argot": {
      "version": "0.6.20"
    },
    "spofford_client": {
      "version": "0.2.2"
    }
  },
  "data": {
    "queue": {
      "pending": 0,
      "processed": 396,
      "working": 0,
      "failed": 0
    },
    "ingests": {
      "summary": {
        "total": 132,
        "completed": 132,
        "failed": 0,
        "last": {
          "id": 132,
          "records": 249,
          "status": 2,
          "created_at": "2018-12-14T13:33:52.118Z",
          "updated_at": "2018-12-14T13:34:00.475Z"
        }
      },
      "add_update": {
        "total": 80,
        "completed": 80,
        "failed": 0,
        "last": {
          "id": 132,
          "records": 249,
          "status": 2,
          "created_at": "2018-12-14T13:33:52.118Z",
          "updated_at": "2018-12-14T13:34:00.475Z"
        }
      },
      "delete": {
        "total": 52,
        "completed": 52,
        "failed": 0,
        "last": {
          "id": 130,
          "records": 2,
          "status": 2,
          "created_at": "2018-12-14T12:32:09.809Z",
          "updated_at": "2018-12-14T12:32:18.339Z"
        }
      }
    }
  }
}
```

#### HTML Version

There is also a human-readable rendering of status information:

    GET /status.html
