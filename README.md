# TRLN Discovery Ingest

A Rails 6 API-only app that implements add/update and delete ingest processes for DUL Libraries implementation of TRLN Discovery.

See [API](./API.md) for details of the API methods.

## Authentication and authorization

Users are authenticated via Shibboleth SSO and authorized by membership in the `perkins-trln-discovery` GroupManager group.
