class XmllintCommand
  class Error < CommandError; end

  #
  # Runs xmllint command.
  #
  # Returns boolean success flag
  #
  def self.run(infile, outfile)
    errmsg = IO.popen("xmllint --recover --output #{outfile} #{infile}", err: %i[child out]) do |io|
      io.read
    end

    $?.success? || raise(Error, "xmllint command exited with non-zero status (#{$?.exitstatus}): #{errmsg}")
  end
end
