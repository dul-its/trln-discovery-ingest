class SpoffordIngestCommand
  class Error < CommandError; end

  #
  # Executes spofford client ingest
  #
  # Returns boolean  success flag
  #
  def self.run(path)
    config = Rails.root.join('config', 'spofford', '%s.yml' % Rails.env)

    cmd = "bundle exec spofford ingest --config #{config} #{path}"

    out = IO.popen(cmd, err: %i[child out]) { |io| io.read }

    raise Error, "Spofford ingest command failed: #{out}" unless $?.success?

    Rails.logger.debug { out }
  end
end
