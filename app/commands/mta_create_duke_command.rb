class MtaCreateDukeCommand
  class Error < CommandError; end

  #
  # Executes MARC-to-Argot conversion
  #
  # Returns boolean success flag
  #
  def self.run(in_path, out_path)
    errmsg = IO.popen("bundle exec mta create duke #{in_path} #{out_path}", err: %i[child out]) do |io|
      io.read
    end

    $?.success? || raise(Error, "MARC-to-Argot conversion failed: #{errmsg}")
  end
end
