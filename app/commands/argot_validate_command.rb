class ArgotValidateCommand
  #
  # Executes Argot validate command.
  #
  # Returns boolean success flag.
  #
  def self.run(r_io)
    IO.popen('bundle exec argot validate -qv', in: r_io, err: %i[child out]) do |io|
      while line = io.gets
        Rails.logger.error { line.chomp }
      end
    end

    $?.success?
  end
end
