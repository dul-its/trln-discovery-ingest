class IngestJob < ApplicationJob
  def perform(ingest)
    processor = "#{ingest.class}Processor".constantize
    processor.process(ingest)
  end
end
