class StatusController < ApplicationController
  include ActionController::MimeResponds
  include ActionView::Rendering

  def index
    @status = Status.new
    respond_to do |format|
      format.html { render :index }
      format.json { render json: @status, status: :ok }
    end
  end
end
