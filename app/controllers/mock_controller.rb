class MockController < ApplicationController
  def create
    render plain: '202 Accepted', status: :accepted
  end
end
