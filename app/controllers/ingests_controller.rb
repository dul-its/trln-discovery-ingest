class IngestsController < ApplicationController
  class InfileError < ::StandardError; end

  class_attribute :infile_pattern

  rescue_from InfileError do |e|
    render json: { error: e }, status: :bad_request
  end

  def create
    set_infile
    set_ingest
    queue_job
    render json: @ingest, status: :created
  end

  private

  def queue_job
    IngestJob.perform_later(@ingest)
  end

  def set_infile
    @infile = params.require(:infile)
    validate_filename!
  end

  def set_ingest
    @ingest = model.new
    @ingest.infile.attach(@infile)
    @ingest.save!
  end

  def validate_filename!
    return if @infile.original_filename =~ infile_pattern

    raise InfileError,
          "infile file name '#{@infile.original_filename}' does not match " \
          "required pattern for #{model} ingests: #{infile_pattern}"
  end

  def model
    controller_name.classify.constantize
  end
end
