module IngestAdmin
  def self.call(model)
    model.rails_admin do
      list do
        field :id
        field :type do
          pretty_value { value.sub(/Ingest/, '') }
        end
        field :created_at do
          date_format :db
        end
        field :records
        field :status do
          pretty_value { I18n.t("tdi.ingest.status.#{value}") }
        end
      end

      show do
        field :id
        field :type do
          pretty_value { value.sub(/Ingest/, '') }
        end
        field :created_at do
          date_format :db
        end
        field :records
        field :status do
          pretty_value { I18n.t("tdi.ingest.status.#{value}") }
        end
        field :infile do
          pretty_value do
            if value.present?
              path = bindings[:view].main_app.rails_blob_path(value)
              bindings[:view].content_tag(:a, value.filename, href: path)
            end
          end
        end
        field :argot do
          pretty_value do
            if value.present?
              path = bindings[:view].main_app.rails_blob_path(value)
              bindings[:view].content_tag(:a, value.filename, href: path)
            end
          end
        end
        field :updated_at do
          date_format :db
        end
      end
    end
  end
end
