class Status
  include ActiveModel::Model

  Data = Struct.new(:queue, :ingests)

  QUEUE_INFO = %i[pending processed working failed]

  attr_reader :meta, :data

  def initialize
    @meta = TrlnDiscoveryIngest::META
    @data = Data.new(queue, ingests)
  end

  def queue
    {} # FIXME: for Sidekiq?
  end

  def ingests
    { all: Ingest.stats,
      add_update: AddUpdateIngest.stats,
      delete: DeleteIngest.stats }
  end
end
