class Ingest < ApplicationRecord
  has_one_attached :infile # Original file received by POST
  has_one_attached :argot  # Argot file for submission to TRLN ingest server

  # status constants
  ACCEPTED  = 0  # default
  FAILED    = 1  # an exception occurred in processing or ingest to TRLN
  COMPLETED = 2  # no errors occurred in processing or ingest to TRLN

  scope :failed, -> { where(status: FAILED) }
  scope :completed, -> { where(status: COMPLETED) }
  scope :older_than_days, ->(num) { where('created_at < ?', Time.current - num.days) }

  Stats = Struct.new(:total, :completed, :failed, :last)

  MIN_WEED_DAYS = 7

  def self.stats
    Stats.new(count, completed.count, failed.count, last)
  end

  def self.weed(num_days = nil)
    num = [MIN_WEED_DAYS, num_days.to_i].max
    older_than_days(num).destroy_all
  end

  def to_s
    format('%s <infile: %s, records: %s, status: %s, created: %s>', type, infile_filename, records || '[null]',
           status_label, created_at.localtime.to_s(:db))
  end

  def completed!
    update! status: COMPLETED
  end

  def completed?
    status == COMPLETED
  end

  def failed!
    update! status: FAILED
  end

  def failed?
    status == FAILED
  end

  def status_label
    I18n.t("tdi.ingest.status.#{status}")
  end

  def infile_filename
    infile.filename.to_s
  rescue StandardError
    nil
  end

  IngestAdmin.call(self)
end
