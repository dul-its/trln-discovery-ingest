require 'zlib'

class AddUpdateIngestProcessor < IngestProcessor
  ENCODE_OPTIONS = { invalid: :replace, undef: :replace }

  # String to use for replacing invalid bytes and non-printing chars
  REPL_INVALID = "\uFFFD" # '�'

  # Non-printing characters (excluding space and newline)
  NON_PRINTING = /[^[:print:]\n]/

  RECORD_REGEX = Regexp.new('<controlfield tag="001">DUKE\d+</controlfield>')

  def process_infile
    scrub
    update_records
    normalize
    convert
    validate
  end

  def gzipped?
    File.extname(infile_filename) == '.gz'
  end

  def update_records
    ingest.update!(records: count_records)
  end

  def count_records
    File.open(scrubbed_path, 'r') do |f|
      f.count { |line| line.match?(RECORD_REGEX) }
    end
  end

  # @raise [XmllintCommand::Error]
  def normalize
    XmllintCommand.run(scrubbed_path, normalized_path)
  end

  # @raise [MtaCreateDukeCommand::Error]
  def convert
    MtaCreateDukeCommand.run(normalized_path, argot_path)
  end

  def validate
    valid? || Rails.logger.warn('Argot validation failed.')
  end

  def valid?
    ArgotValidateCommand.run(argot_path)
  end

  def scrub_line(line)
    line.encode('UTF-8', **ENCODE_OPTIONS).gsub(NON_PRINTING, REPL_INVALID)
  end

  def scrub
    io = gzipped? ? Zlib::GzipReader.open(infile_path) : File.open(infile_path, 'r')
    File.open(scrubbed_path, 'w') do |scrubbed|
      io.each do |line|
        scrubbed.puts scrub_line(line)
      end
    end
  end

  def scrubbed_path
    File.join(dir, 'scrubbed.xml')
  end

  def normalized_path
    File.join(dir, 'normalized.xml')
  end
end
