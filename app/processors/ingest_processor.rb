require 'tempfile'

class IngestProcessor
  include ActiveModel::Model

  class_attribute :retry_attempts, :retry_delay
  self.retry_attempts = 3
  self.retry_delay = 60

  attr_accessor :ingest, :dir

  delegate :completed?, to: :ingest

  define_model_callbacks :process

  before_process :abort_if_completed, if: :completed?

  def self.process(ingest)
    new(ingest:).process
  end

  def process
    run_callbacks :process do
      create_workdir
      begin
        process_ingest
      rescue Exception => e
        handle_exception(e)
      ensure
        remove_workdir
      end
    end
  end

  def create_workdir
    self.dir = Dir.mktmpdir
  end

  def remove_workdir
    FileUtils.remove_entry_secure(dir) if dir
  end

  def process_ingest
    unless ingest.argot.attached?
      download_infile
      process_infile
      attach_argot
    end
    ingest_argot
    complete
  end

  def complete
    ingest.completed!
    logger.info "Ingest successfully processed: #{ingest}"
  end

  def process_infile
    raise NotImplementedError
  end

  def attach_argot
    ingest.argot.attach(io: File.open(argot_path),
                        filename: argot_filename,
                        content_type: 'application/json')
  end

  # @raise [SpoffordIngestCommand::Error]
  def ingest_argot
    File.exist?(argot_path) || download_argot

    tries = 0
    begin
      SpoffordIngestCommand.run(argot_path)
    rescue SpoffordIngestCommand::Error => e
      raise unless (tries += 1) < retry_attempts

      logger.error e.full_message
      sleep retry_delay
      retry
    end
  end

  def handle_exception(e)
    logger.error "Ingest processing failed: #{e}"
    ingest.failed!
    raise
  end

  def download_infile
    File.open(infile_path, 'wb') do |f|
      ingest.infile.download { |chunk| f.write(chunk) }
    end
  end

  def download_argot
    File.open(argot_path, 'wb') do |f|
      ingest.argot.download { |chunk| f.write(chunk) }
    end
  end

  def infile_filename
    ingest.infile.filename.to_s
  end

  def infile_path
    File.join(dir, infile_filename)
  end

  def argot_filename
    "#{ingest.infile.filename.base}.json"
  end

  def argot_path
    File.join(dir, argot_filename)
  end

  private

  def logger
    Rails.logger
  end

  def abort_if_completed
    logger.warn "Ingest already completed, will not process again: #{ingest}"
    throw :abort
  end
end
