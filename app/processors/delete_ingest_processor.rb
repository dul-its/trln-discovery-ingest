class DeleteIngestProcessor < IngestProcessor
  def process_infile
    records = File.read(infile_path).strip.lines.map(&:chomp)
    ingest.update!(records: records.length)
    File.write(argot_path, records.to_json)
  end
end
