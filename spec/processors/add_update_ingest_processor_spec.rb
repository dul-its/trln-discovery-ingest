require 'rails_helper'
require 'support/shared_examples_for_ingest_processors'

RSpec.describe AddUpdateIngestProcessor do
  subject { described_class.new(ingest:) }

  let(:ingest) { AddUpdateIngest.create!(infile:) }

  describe 'with a regular file' do
    let(:infile) { fixture_file_upload('add_test.xml') }

    it_behaves_like 'an ingest processor'

    it 'updates the number of records for the ingest in the database' do
      allow(subject).to receive(:ingest_argot).and_return(nil)
      expect { subject.process }.to change { subject.ingest.records }.from(nil).to(26)
    end
  end

  describe 'with a gzipped file' do
    let(:infile) { fixture_file_upload('add_test.xml.gz') }

    it_behaves_like 'an ingest processor'

    it 'updates the number of records for the ingest in the database' do
      allow(subject).to receive(:ingest_argot).and_return(nil)
      expect { subject.process }.to change { subject.ingest.records }.from(nil).to(26)
    end
  end

  describe 'handling bad UTF-8 bytes' do
    let(:infile) { fixture_file_upload('add_bad_utf8.xml') }

    before do
      subject.create_workdir
      subject.download_infile
    end

    after { subject.remove_workdir }

    it 'fixes the encoding to be valid UTF-8' do
      original_infile = File.read(subject.infile_path)
      expect(original_infile.valid_encoding?).to be false
      subject.scrub
      scrubbed_infile = File.read(subject.scrubbed_path)
      expect(scrubbed_infile.encoding).to eq Encoding::UTF_8
      expect(scrubbed_infile.valid_encoding?).to be true
    end
  end

  describe 'handling dirty XML' do
    let(:infile) { fixture_file_upload('add_urls_dirty.xml') }

    before do
      subject.create_workdir
      subject.download_infile
    end

    after { subject.remove_workdir }

    describe 'process_infile' do
      xit 'handles the ampersand properly' do
        subject.process_infile
        matches = false
        File.open(subject.argot_path, 'r').each do |line|
          matches = true if line =~ %r{http://link\.overdrive\.com/\?websiteID=201153\\\\u0026titleID=3750302}
        end
        expect(matches).to be true
      end
    end

    describe 'scrub' do
      it 'succeeds' do
        subject.scrub
        expect(File.read(subject.scrubbed_path)).to match(%r{http://link\.overdrive\.com/\?websiteID=201153&amp;titleID=3750302})
      end
    end
  end

  describe 'ampersands in proxy URLs' do
    let(:infile) { fixture_file_upload('url_test.xml') }

    before do
      allow(SpoffordIngestCommand).to receive(:run).and_return(true)
    end

    it 'scrubs the data' do
      expect(subject).to receive(:scrub).and_call_original
      subject.process
    end

    it 'normalizes the XML' do
      expect(subject).to receive(:normalize).and_call_original
      subject.process
    end

    it 'converts the XML to Argot' do
      expect(subject).to receive(:convert).and_call_original
      subject.process
    end

    xit 'handles the ampersand properly' do
      subject.process
      expect(ingest.argot.download)
        .to match(%r{http://link\.overdrive\.com/\?websiteID=201153\\\\u0026titleID=3750302})
    end

    describe 'process steps' do
      before do
        subject.create_workdir
        subject.download_infile
      end

      after { subject.remove_workdir }

      describe 'scrub' do
        it 'succeeds' do
          subject.scrub
          expect(File.read(subject.scrubbed_path)).to match(%r{http://link\.overdrive\.com/\?websiteID=201153&amp;titleID=3750302})
        end
      end

      describe 'normalize' do
        before { subject.scrub }

        it 'succeeds' do
          subject.normalize
          expect(File.read(subject.normalized_path)).to match(%r{http://link\.overdrive\.com/\?websiteID=201153&amp;titleID=3750302})
        end
      end

      describe 'convert' do
        before do
          subject.scrub
          subject.normalize
        end

        xit 'succeeds' do
          subject.convert
          expect(File.read(subject.argot_path))
            .to match(%r{http://link\.overdrive\.com/\?websiteID=201153\\\\u0026titleID=3750302})
        end
      end
    end
  end
end
