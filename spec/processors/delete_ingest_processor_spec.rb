require 'rails_helper'
require 'support/shared_examples_for_ingest_processors'

RSpec.describe DeleteIngestProcessor do
  subject { described_class.new(ingest:) }

  let(:infile) { fixture_file_upload('delete_test.txt') }
  let(:ingest) { DeleteIngest.create!(infile:) }

  it_behaves_like 'an ingest processor'

  it 'updates the number of records for the ingest in the database' do
    allow(subject).to receive(:ingest_argot).and_return(nil)
    expect { subject.process }.to change { subject.ingest.records }.from(nil).to(2)
  end
end
