require 'rails_helper'

RSpec.describe MockController do
  describe '#create' do
    it 'responds with 202 Accepted status' do
      post :create
      expect(response.status).to eq 202
    end
  end
end
