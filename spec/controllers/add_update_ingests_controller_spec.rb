require 'rails_helper'
require 'digest'

RSpec.describe AddUpdateIngestsController, type: :controller do
  let(:sha1) { Digest::SHA1.file(infile.path).digest }

  before do
    allow(SpoffordIngestCommand).to receive(:run).and_return(true)
  end

  describe 'create' do
    before do
      post :create, params: { infile: }
    end

    describe 'with a regular file' do
      let(:infile) { fixture_file_upload('add_test.xml') }

      it 'returns a 201 created response' do
        expect(response.status).to eq(201)
      end

      it 'sets the record count of the infile' do
        ingest = AddUpdateIngest.last
        expect(ingest.records).to eq(26)
      end

      it 'attaches the infile' do
        ingest = AddUpdateIngest.last
        expect(ingest.infile).to be_attached
        expect(Digest::SHA1.digest(ingest.infile.download)).to eq(sha1)
      end

      it 'attaches the Argot file' do
        ingest = AddUpdateIngest.last
        expect(ingest.argot).to be_attached
        expect(ingest.argot.filename.base).to eq(ingest.infile.filename.base)
      end
    end

    describe 'with a gzipped file' do
      let(:infile) { fixture_file_upload('add_test.xml.gz') }

      it 'returns a 201 created response' do
        expect(response.status).to eq(201)
      end

      it 'sets the record count of the infile' do
        ingest = AddUpdateIngest.last
        expect(ingest.records).to eq(26)
      end

      it 'attaches the infile' do
        ingest = AddUpdateIngest.last
        expect(ingest.infile).to be_attached
        expect(Digest::SHA1.digest(ingest.infile.download)).to eq(sha1)
      end

      it 'attaches the Argot file' do
        ingest = AddUpdateIngest.last
        expect(ingest.argot).to be_attached
        expect(ingest.argot.filename.base).to eq(ingest.infile.filename.base)
      end
    end
  end
end
