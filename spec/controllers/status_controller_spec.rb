require 'rails_helper'

RSpec.describe StatusController, type: :controller, status: true do
  describe 'json' do
    it 'succeeds' do
      get :index, format: 'json'
      expect(response).to be_successful
    end

    it 'contains the TDI version' do
      get :index, format: 'json'
      content = JSON.parse(response.body)
      expect(content['meta']['tdi']['version']).to eq(TrlnDiscoveryIngest::VERSION)
    end

    it 'contains the Spofford Client version' do
      get :index, format: 'json'
      content = JSON.parse(response.body)
      expect(content['meta']['spofford_client']['version']).to eq(Spofford::Client::VERSION)
    end

    it 'contains the argot version' do
      get :index, format: 'json'
      content = JSON.parse(response.body)
      expect(content['meta']['argot']['version']).to eq(Argot::VERSION)
    end

    it 'contains the marc_to_argot version' do
      get :index, format: 'json'
      content = JSON.parse(response.body)
      expect(content['meta']['marc_to_argot']['version']).to eq(MarcToArgot::VERSION)
    end
  end

  describe 'html' do
    it 'succeeds' do
      get :index, format: 'html'
      expect(response).to be_successful
    end
  end
end
