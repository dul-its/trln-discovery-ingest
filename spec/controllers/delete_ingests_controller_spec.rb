require 'rails_helper'
require 'digest'

RSpec.describe DeleteIngestsController, type: :controller do
  let(:infile) { fixture_file_upload('delete_test.txt') }
  let(:sha1) { Digest::SHA1.file(infile.path).digest }

  before do
    allow(SpoffordIngestCommand).to receive(:run).and_return(true)
  end

  describe 'create' do
    before do
      post :create, params: { infile: }
    end

    it 'returns a 201 created response' do
      expect(response.status).to eq(201)
    end

    it 'sets the record count of the infile' do
      ingest = DeleteIngest.last
      expect(ingest.records).to eq(2)
    end

    it 'attaches the infile' do
      ingest = DeleteIngest.last
      expect(ingest.infile).to be_attached
      expect(Digest::SHA1.digest(ingest.infile.download)).to eq(sha1)
    end

    it 'attaches the Argot file' do
      ingest = DeleteIngest.last
      expect(ingest.argot).to be_attached
      expect(JSON.parse(ingest.argot.download)).to eq %w[DUKE003230289 DUKE004657475]
    end
  end
end
