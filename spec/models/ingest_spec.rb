require 'rails_helper'

RSpec.describe Ingest, type: :model do
  describe '.weed' do
    it 'weeds old ingests' do
      ingest1 = AddUpdateIngest.create(created_at: Time.current - 14.days)
      ingest2 = AddUpdateIngest.create
      described_class.weed
      expect { ingest1.reload }.to raise_error(ActiveRecord::RecordNotFound)
      expect { ingest2.reload }.not_to raise_error
    end
  end
end
