require 'rails_helper'

RSpec.describe IngestJob, type: :job do
  describe 'retry on Spofford error' do
    let(:ingest) { AddUpdateIngest.create }

    before do
      allow(AddUpdateIngestProcessor).to receive(:process).with(ingest).and_raise(SpoffordIngestCommand::Error)
    end

    it 'retries 3 times' do
      described_class.perform_later(ingest)
      expect(AddUpdateIngestProcessor).to receive(:process).with(ingest).exactly(4)
    rescue SpoffordIngestCommand::Error => _e
    end
  end
end
