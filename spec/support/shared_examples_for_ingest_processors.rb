RSpec.shared_examples 'an ingest processor' do
  describe 'when the status is COMPLETED (2)' do
    before { ingest.completed! }

    it 'does not process the ingest again' do
      expect(subject).not_to receive(:process_ingest)
      subject.process
    end
  end

  describe 'when argot file is not attached' do
    before do
      allow(SpoffordIngestCommand).to receive(:run).and_return(true)
    end

    it 'downloads the infile' do
      expect(subject).to receive(:download_infile).and_call_original
      subject.process
    end

    it 'processes the infile' do
      expect(subject).to receive(:process_infile).and_call_original
      subject.process
    end

    it 'attaches the argot file' do
      expect(subject.ingest.argot).to receive(:attach).and_call_original
      subject.process
    end

    it 'does not download the argot file' do
      expect(subject).not_to receive(:download_argot)
      subject.process
    end
  end

  describe 'when argot file is attached' do
    let(:argot_file) { fixture_file_upload('add_test.json') }

    before do
      ingest.argot.attach(argot_file)
      allow(SpoffordIngestCommand).to receive(:run).and_return(true)
    end

    it 'does not download the infile' do
      expect(subject).not_to receive(:download_infile)
      subject.process
    end

    it 'does not process the infile' do
      expect(subject).not_to receive(:process_infile)
      subject.process
    end

    it 'does not attach the argot file' do
      expect(subject.ingest.argot).not_to receive(:attach)
      subject.process
    end

    it 'downloads the argot file' do
      expect(subject).to receive(:download_argot)
      subject.process
    end
  end
end
